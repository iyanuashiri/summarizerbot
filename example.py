from app import db
from app import create_app
from app.models import Summary

app = create_app()
app.app_context().push()

s = Summary("", "", "")
db.session.add(s)
db.session.commit()

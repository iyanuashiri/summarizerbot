from time import sleep
import os
import logging

import tweepy
import threader
from goose3 import Goose
from celery import Celery
from celery import shared_task
from dotenv import load_dotenv, find_dotenv

from config import REDIS_URL, redis_db, CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN_SECRET, ACCESS_TOKEN, SINCE_ID
from app import create_app
from app.summarize import summarize_article, summarize_thread
from app.models import Summary
from app import db


celery = Celery('app.bot', broker=REDIS_URL or 'redis://localhost:6379/0',
                backend=REDIS_URL or 'redis://localhost:6379/0')


celery.conf.update(result_expires=3600)

celery.autodiscover_tasks()


app = create_app()
app.app_context().push()

load_dotenv(find_dotenv())

logger = logging.getLogger()


@shared_task
def post_article(screen_name, url, sentences_count, in_reply_to_status_id):
    username = screen_name
    threaded = threader.Thread(connect_api())
    summary = summarize_article(url, sentences_count)
    title = get_title(url)

    s = Summary(title=title, summary=summary, url=url)
    db.session.add(s)
    db.session.commit()
    domain_name = 'https://summarizerbot.herokoapp.com'
    reply = f'Read the summary you asked for on this link {domain_name}/blog/{s.id} below'
    # thread = threaded.post_thread(reply, f'@{username}', in_reply_to_status_id)
    thread = connect_api().update_status(f'@{username} {reply}', in_reply_to_status_id)
    return {'message': f'@{username} {reply}'}


@celery.task
def post_thread(screen_name, status_id, sentences_count, in_reply_to_status_id):
    username = screen_name
    threaded = threader.Thread(connect_api())
    thread = threaded.convert_to_post(status_id)
    summary = summarize_thread(thread, sentences_count)

    # It does not matter. Twitter redirects to the right status id
    url = f'https://twitter.com/itdoesnotmatter/status/{status_id}'
    s = Summary(title=summary[0:20], summary=summary, url=url)
    db.session.add(s)
    db.session.commit()
    domain_name = 'https://summarizerbot.herokoapp.com'
    reply = f'Read the summary you asked for on this link {domain_name}/blog/{s.id} below'
    # thread = thread.post_thread(reply, f'@{username}', in_reply_to_status_id)
    thread = connect_api().update_status(f'@{username} {reply}', in_reply_to_status_id)
    return {'message': f'@{username} {reply}'}


def connect_api():
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    return api


def get_latest_mentions(api, since_id):
    latest_id = int(since_id)
    for tweet in tweepy.Cursor(api.mentions_timeline, since_id=since_id).items():
        latest_id = max(tweet.id, latest_id)

        redis_db.set('since_id', latest_id)
        logger.info('Setting since_id in redis...')
        hashtags, in_reply_to_status_id, screen_name = tweet.entities["hashtags"], tweet.in_reply_to_status_id, \
                                                       tweet.user.screen_name

        sentence_count = 5

        text = ''
        try:
            text = str(hashtags[0]['text'])
        except IndexError:
            pass

        # Get Original Tweet
        status = api.get_status(in_reply_to_status_id, tweet_mode='extended')
        status_id = status.id

        url = ''
        try:
            urls = status.entities["urls"]
            url = urls[0]["expanded_url"]
        except IndexError:
            pass

        if text == 'thread':
            post_thread.delay(screen_name, status_id, sentence_count, in_reply_to_status_id)
        elif text == 'article':
            post_article.delay(screen_name, url, sentence_count, in_reply_to_status_id)

    return latest_id


def get_title(url):
    g = Goose()
    article = g.extract(url=url)
    title = article.title
    return title


def main():
    api = connect_api()
    set_redis = redis_db.set('since_id', SINCE_ID)
    since_id = int(redis_db.get('since_id'))
    print("Get the first since_id...")
    logger.info('Get the first since_id')
    while True:
        # since_id = int(redis_db.get('since_id'))
        try:
            print('Getting mentions...')
            logger.info('Getting mentions...')
            since_id = get_latest_mentions(api, since_id)
            # redis_db.set('since_id', latest_id)
            logger.info('Sleeping...in 1 minute')
            print('Sleeping...in 1 minute')
            sleep(60)
        except tweepy.TweepError:
            logger.info('Sleeping...in 15 minutes')
            print('Sleeping...in 15 minutes')
            sleep(60 * 15)
            continue


if __name__ == '__main__':
    main()
